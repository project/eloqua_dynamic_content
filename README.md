Eloqua Dynamic Content
======================

Provides an input filter to embed Eloqua [Dynamic Content](https://docs.oracle.com/cloud/latest/marketingcs_gs/OMCAA/index.html#Help/DynamicContent/DynamicContent.htm%3FTocPath%3DComponent%2520Library%7CDynamic%2520Content%7C_____0)
into landing pages on your Drupal site without adding or editing any
JavaScript.

Installation
============

- Follow the [standard module installation guide](https://www.drupal.org/docs/7/extending-drupal/installing-modules)
to install the Eloqua Dynamic Content and [Eloqua Asynchronous Visitor Tracking](https://www.drupal.org/project/eloqua_tracking)
modules.
- Configure the Eloqua Asynchronous Visitor Tracking module.
- Enable the <i>Eloqua Dynamic Content</i> filter for your text formats.
  
Usage
=====

- Follow Eloqua's [documentation on embedding dynamic content in an external landing page](https://docs.oracle.com/cloud/latest/marketingcs_gs/OMCAA/index.html#Help/DynamicContent/Tasks/EmbeddingDynamicContentInExternalLP.htm%3FTocPath%3DComponent%2520Library%7CDynamic%2520Content%7C_____8).
At step 3.III, don't copy the JavaScript code, but only the two numbers
in the line that looks like `elq.dynamic(-500004, 1702995451), 'replace-me')`. 
- Edit your content on your Drupal site and add the following  tag
`[elq.dynamic:-500004, 1702995451]` where you want the dynamic content
to appear (replace the two numbers with those from the previous step).   