(function($){
  Drupal.behaviors.EloquaDynamicContent = {
    attach: function(context, settings) {
      $('div[data-elq-dynamic]').each(function (index) {
        var matches = $(this).data('elq-dynamic').split('-');
        elq.dynamic(matches[0], matches[1], $(this).attr('id'));
      });
    }
  };
})(jQuery);
